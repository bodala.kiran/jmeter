/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.7313432835820896, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-23"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-22"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-21"], "isController": false}, {"data": [0.9, 500, 1500, "https://demowebshop.tricentis.com/logout-20"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/login-18"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-19"], "isController": false}, {"data": [0.0, 500, 1500, "https://demowebshop.tricentis.com/login"], "isController": false}, {"data": [0.0, 500, 1500, "https://demowebshop.tricentis.com/logout"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-23"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-24"], "isController": false}, {"data": [0.0, 500, 1500, "Test"], "isController": true}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-21"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-22"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-24"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-20"], "isController": false}, {"data": [0.7, 500, 1500, "https://demowebshop.tricentis.com/login-2"], "isController": false}, {"data": [0.7, 500, 1500, "https://demowebshop.tricentis.com/login-1"], "isController": false}, {"data": [0.7, 500, 1500, "https://demowebshop.tricentis.com/login-4"], "isController": false}, {"data": [0.7, 500, 1500, "https://demowebshop.tricentis.com/login-3"], "isController": false}, {"data": [0.65, 500, 1500, "https://demowebshop.tricentis.com/login-6"], "isController": false}, {"data": [0.7, 500, 1500, "https://demowebshop.tricentis.com/login-5"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/login-8"], "isController": false}, {"data": [0.6, 500, 1500, "https://demowebshop.tricentis.com/login-7"], "isController": false}, {"data": [0.55, 500, 1500, "https://demowebshop.tricentis.com/login-0"], "isController": false}, {"data": [0.6, 500, 1500, "https://demowebshop.tricentis.com/logout-5"], "isController": false}, {"data": [0.6, 500, 1500, "https://demowebshop.tricentis.com/logout-4"], "isController": false}, {"data": [0.8, 500, 1500, "https://demowebshop.tricentis.com/logout-3"], "isController": false}, {"data": [0.7, 500, 1500, "https://demowebshop.tricentis.com/logout-2"], "isController": false}, {"data": [0.6, 500, 1500, "https://demowebshop.tricentis.com/logout-1"], "isController": false}, {"data": [0.9, 500, 1500, "https://demowebshop.tricentis.com/logout-0"], "isController": false}, {"data": [0.8, 500, 1500, "https://demowebshop.tricentis.com/logout-9"], "isController": false}, {"data": [0.6, 500, 1500, "https://demowebshop.tricentis.com/logout-8"], "isController": false}, {"data": [0.8, 500, 1500, "https://demowebshop.tricentis.com/logout-7"], "isController": false}, {"data": [0.6, 500, 1500, "https://demowebshop.tricentis.com/logout-6"], "isController": false}, {"data": [0.85, 500, 1500, "https://demowebshop.tricentis.com/login-9"], "isController": false}, {"data": [0.6, 500, 1500, "https://demowebshop.tricentis.com/logout-12"], "isController": false}, {"data": [0.7, 500, 1500, "https://demowebshop.tricentis.com/logout-11"], "isController": false}, {"data": [0.7, 500, 1500, "https://demowebshop.tricentis.com/logout-10"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-16"], "isController": false}, {"data": [0.6, 500, 1500, "https://demowebshop.tricentis.com/login-17"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-19"], "isController": false}, {"data": [0.8, 500, 1500, "https://demowebshop.tricentis.com/login-14"], "isController": false}, {"data": [0.9, 500, 1500, "https://demowebshop.tricentis.com/logout-18"], "isController": false}, {"data": [0.9, 500, 1500, "https://demowebshop.tricentis.com/login-15"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-17"], "isController": false}, {"data": [0.85, 500, 1500, "https://demowebshop.tricentis.com/login-12"], "isController": false}, {"data": [0.8, 500, 1500, "https://demowebshop.tricentis.com/logout-16"], "isController": false}, {"data": [0.7, 500, 1500, "https://demowebshop.tricentis.com/login-13"], "isController": false}, {"data": [0.7, 500, 1500, "https://demowebshop.tricentis.com/logout-15"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/login-10"], "isController": false}, {"data": [0.9, 500, 1500, "https://demowebshop.tricentis.com/logout-14"], "isController": false}, {"data": [0.8, 500, 1500, "https://demowebshop.tricentis.com/login-11"], "isController": false}, {"data": [0.7, 500, 1500, "https://demowebshop.tricentis.com/logout-13"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 330, 0, 0.0, 641.3060606060607, 175, 4956, 286.0, 1249.8000000000018, 2338.199999999999, 3935.449999999999, 10.12487343908201, 353.1391182539195, 18.603316375448713], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["https://demowebshop.tricentis.com/logout-23", 5, 0, 0.0, 198.6, 185, 211, 197.0, 211.0, 211.0, 211.0, 0.6220452848967405, 0.07532579621796466, 0.5576538784523513], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-22", 5, 0, 0.0, 195.2, 188, 206, 193.0, 206.0, 206.0, 206.0, 0.6205013651030032, 0.0751388371804418, 0.5502101948374286], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-21", 5, 0, 0.0, 191.0, 177, 198, 194.0, 198.0, 198.0, 198.0, 0.621272365805169, 0.07462548925198807, 0.5539274120899602], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-20", 5, 0, 0.0, 316.4, 194, 783, 199.0, 783.0, 783.0, 783.0, 0.5770340450086555, 0.07043872619734565, 0.5071588286208887], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-18", 5, 0, 0.0, 1226.6, 1010, 1488, 1255.0, 1488.0, 1488.0, 1488.0, 0.6688068485821295, 287.86583212112095, 0.7641640750401284], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-19", 5, 0, 0.0, 200.6, 192, 215, 196.0, 215.0, 215.0, 215.0, 0.7659313725490196, 2.2933062385110294, 0.8945839077818628], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login", 10, 0, 0.0, 3514.6, 2722, 4956, 3184.0, 4928.5, 4956.0, 4956.0, 0.41324021653787346, 229.96818050332658, 8.017223400243811], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout", 5, 0, 0.0, 2714.0, 2387, 3189, 2611.0, 3189.0, 3189.0, 3189.0, 0.46926325668700136, 17.825588045518536, 10.244970084467386], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-23", 5, 0, 0.0, 206.4, 191, 217, 207.0, 217.0, 217.0, 217.0, 0.7655795437145919, 2.945686916245598, 0.9038922542489665], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-24", 5, 0, 0.0, 207.0, 193, 217, 210.0, 217.0, 217.0, 217.0, 0.7760360080707744, 2.9866776443426977, 0.9025965679807544], "isController": false}, {"data": ["Test", 5, 0, 0.0, 9743.2, 8736, 10555, 9806.0, 10555.0, 10555.0, 10555.0, 0.4737091425864519, 545.2327466248224, 28.722780228564663], "isController": true}, {"data": ["https://demowebshop.tricentis.com/login-21", 5, 0, 0.0, 203.6, 193, 218, 203.0, 218.0, 218.0, 218.0, 0.7749535027898327, 2.98251147899876, 0.9119325887321761], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-22", 5, 0, 0.0, 208.2, 195, 217, 211.0, 217.0, 217.0, 217.0, 0.7757951900698216, 2.9963573991466252, 0.9083773758727696], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-24", 5, 0, 0.0, 198.0, 181, 211, 200.0, 211.0, 211.0, 211.0, 0.6222775357809583, 0.0759616132545115, 0.5475313083385189], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-20", 5, 0, 0.0, 207.2, 196, 229, 202.0, 229.0, 229.0, 229.0, 0.7739938080495357, 5.756578947368421, 0.8994654605263158], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-2", 10, 0, 0.0, 692.5, 197, 1125, 814.5, 1122.5, 1125.0, 1125.0, 0.4836759371221282, 6.060119407496977, 0.47682701027811364], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-1", 10, 0, 0.0, 677.3, 385, 1343, 547.5, 1337.0, 1343.0, 1343.0, 0.503068719187041, 31.710524040396418, 0.4647490315927156], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-4", 10, 0, 0.0, 845.4000000000001, 196, 1434, 1066.0, 1427.0, 1434.0, 1434.0, 0.4836525440123815, 22.336104133415557, 0.48011016698104086], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-3", 10, 0, 0.0, 712.3999999999999, 198, 1138, 918.5, 1137.0, 1138.0, 1138.0, 0.4836759371221282, 6.476250755743652, 0.48367593712212814], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-6", 10, 0, 0.0, 714.9, 204, 1032, 870.5, 1024.0, 1032.0, 1032.0, 0.48393341076267904, 1.9811024003097173, 0.46502976190476186], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-5", 10, 0, 0.0, 677.2, 200, 1159, 777.5, 1154.5, 1159.0, 1159.0, 0.48386316349736297, 5.795253680384187, 0.4595282485363139], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-8", 10, 0, 0.0, 506.1, 206, 840, 506.0, 834.8000000000001, 840.0, 840.0, 0.5014039310068191, 1.8653303468461693, 0.4906315809265945], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-7", 10, 0, 0.0, 883.5, 194, 2380, 531.0, 2372.4, 2380.0, 2380.0, 0.5035246727089627, 56.19512367824773, 0.49270675981873113], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-0", 10, 0, 0.0, 966.6, 225, 1807, 1104.5, 1778.6000000000001, 1807.0, 1807.0, 0.47687172150691465, 5.5953259120171674, 0.31574123748211735], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-5", 5, 0, 0.0, 667.2, 193, 792, 785.0, 792.0, 792.0, 792.0, 0.6367804381049414, 0.07711013117677025, 0.5640232982042791], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-4", 5, 0, 0.0, 664.2, 196, 797, 776.0, 797.0, 797.0, 797.0, 0.5927682276229994, 0.0712016523414345, 0.5279342027267339], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-3", 5, 0, 0.0, 428.2, 191, 786, 198.0, 786.0, 786.0, 786.0, 0.6363752068219422, 0.07706106020109456, 0.551856624665903], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-2", 5, 0, 0.0, 547.4, 193, 785, 775.0, 785.0, 785.0, 785.0, 0.5921364282330649, 0.0717040206063477, 0.5111802759355755], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-1", 5, 0, 0.0, 536.4, 391, 591, 568.0, 591.0, 591.0, 591.0, 0.6072382803011902, 20.924814412800583, 0.4512776672941462], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-0", 5, 0, 0.0, 359.2, 216, 824, 256.0, 824.0, 824.0, 824.0, 0.6174364040503828, 0.4166489796863423, 0.6596439707335144], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-9", 5, 0, 0.0, 429.2, 191, 781, 199.0, 781.0, 781.0, 781.0, 0.592627711271779, 0.07118477391252816, 0.5133406053692071], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-8", 5, 0, 0.0, 666.8, 200, 790, 782.0, 790.0, 790.0, 790.0, 0.5919962112242482, 0.0716870412029363, 0.5064342588207436], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-7", 5, 0, 0.0, 433.0, 190, 800, 198.0, 800.0, 800.0, 800.0, 0.6363752068219422, 0.07643960003818251, 0.5425347222222222], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-6", 5, 0, 0.0, 706.8, 200, 943, 792.0, 943.0, 943.0, 943.0, 0.581260172053011, 0.07038697395954428, 0.5210906620553359], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-9", 10, 0, 0.0, 393.5, 197, 837, 228.0, 833.7, 837.0, 837.0, 0.5018065034122843, 0.8668903364612605, 0.49102550431553593], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-12", 5, 0, 0.0, 666.0, 207, 802, 769.0, 802.0, 802.0, 802.0, 0.5922767116796968, 0.07172100805496327, 0.5032038468372424], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-11", 5, 0, 0.0, 547.6, 190, 799, 768.0, 799.0, 799.0, 799.0, 0.5798446016467587, 0.07021555723066218, 0.49887020903397894], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-10", 5, 0, 0.0, 547.4, 187, 802, 775.0, 802.0, 802.0, 802.0, 0.6066488716330988, 0.07346138679932054, 0.5237085962145109], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-16", 5, 0, 0.0, 217.2, 192, 306, 195.0, 306.0, 306.0, 306.0, 0.8111615833874107, 0.09664229802076574, 1.0004854295100585], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-17", 5, 0, 0.0, 675.0, 393, 1203, 587.0, 1203.0, 1203.0, 1203.0, 0.7439369141496801, 40.503151270272284, 0.8500060444874274], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-19", 5, 0, 0.0, 195.4, 193, 203, 194.0, 203.0, 203.0, 203.0, 0.6201165819174004, 0.07509224234156021, 0.5480522525734838], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-14", 5, 0, 0.0, 658.4, 373, 1006, 498.0, 1006.0, 1006.0, 1006.0, 0.765814060346148, 22.305082372874868, 0.8922032949149946], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-18", 5, 0, 0.0, 311.6, 194, 775, 197.0, 775.0, 775.0, 775.0, 0.6196554715578139, 0.11981619469574915, 0.5325164208699962], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-15", 5, 0, 0.0, 337.8, 189, 802, 197.0, 802.0, 802.0, 802.0, 0.8103727714748784, 0.09733969813614263, 0.9749797406807131], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-17", 5, 0, 0.0, 196.0, 189, 204, 195.0, 204.0, 204.0, 204.0, 0.6195019204559534, 0.07501781068021311, 0.5317794805476397], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-12", 10, 0, 0.0, 389.2, 175, 791, 221.0, 790.6, 791.0, 791.0, 0.49222287851939356, 1.8242049062315417, 0.4821284640185076], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-16", 5, 0, 0.0, 426.4, 193, 771, 205.0, 771.0, 771.0, 771.0, 0.6201935003721162, 0.07389024125527165, 0.5372183933887373], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-13", 5, 0, 0.0, 561.4, 192, 847, 783.0, 847.0, 847.0, 847.0, 0.6911805363560962, 0.08369764307437103, 0.8423762786839922], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-15", 5, 0, 0.0, 548.2, 205, 787, 769.0, 787.0, 787.0, 787.0, 0.6201165819174004, 0.0744866597420315, 0.5183787051965769], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-10", 10, 0, 0.0, 514.8000000000001, 199, 835, 528.5, 831.7, 835.0, 835.0, 0.4911350130150778, 0.9578092001866313, 0.48034347220175827], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-14", 5, 0, 0.0, 314.4, 192, 778, 199.0, 778.0, 778.0, 778.0, 0.6207324643078833, 0.07516682184978275, 0.5467780105524519], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-11", 10, 0, 0.0, 446.1, 193, 926, 237.0, 916.6, 926.0, 926.0, 0.5067909993918508, 0.38504238039732414, 0.49120123720352726], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-13", 5, 0, 0.0, 544.0, 191, 784, 774.0, 784.0, 784.0, 784.0, 0.5809900069718801, 0.0703542586567511, 0.49474930281199164], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 330, 0, "", "", "", "", "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
